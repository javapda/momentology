// https://momentjs.com/docs/#/parsing/
const moment = require('moment')
moment().format()
describe('ISO 8601 strings', () => {
    const expDateString='Fri Feb 08 2013 00:00:00 GMT-0700'
    test('yyyy-mm-dd calendar date part', () => {
        expect(moment('2013-02-08').toString()).toBe(expDateString)
    })
    test('week date part', () => {
        expect(moment('2013-W06-5').toString()).toBe(expDateString)
    })
    test('ordinal part', () => {
        expect(moment('2013-039').toString()).toBe(expDateString)
    })
    test('Basic (short) full date', () => {
        expect(moment('20130208').toString()).toBe(expDateString)
    })
    test('Basic (short) week, weekday', () => {
        expect(moment('2013W065').toString()).toBe(expDateString)
    })
    test('Basic (short) week only', () => {
        expect(moment('2013W06').toString()).toBe('Mon Feb 04 2013 00:00:00 GMT-0700')
    })
    test('Basic (short) ordinal date', () => {
        expect(moment('2013050').toString()).toBe('Tue Feb 19 2013 00:00:00 GMT-0700')
    })
})



describe('placeholder test', () => {
    test('just a place', () => {
        expect(4).toBe(4)
    });
});