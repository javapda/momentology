# momentology #

* usages of [momentjs](https://momentjs.com/)

# Install / setup #


```
npm install 
yarn test
```

## run tests ##

* with node
```
node run test
```

* with yarn
```
yarn test
```

# resources #

* [moment](https://momentjs.com/) | [docs](https://momentjs.com/docs/)
* [jest](https://jestjs.io/)

# to npmjs #

```
npm publish --access public
npm version patch; npm publish --access public

```